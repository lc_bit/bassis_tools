package bassis.bassis_tools.reflex;

import java.lang.reflect.Method;

public class Reflection {
	public static Method getMethod(Class<?> las, String methodName) throws Exception {
		Method[] methods = las.getDeclaredMethods();
		Method method = null;
		// 方法筛选
		for (Method m : methods) {
			if (m.getName().equals(methodName)) {
				method = m;
				break;
			}
		}
		return method;
	}

	/**
	 * 代执行方法
	 * 
	 * @param method_name
	 *            方法名
	 * @param las
	 *            类名
	 * @return 返回方法执行后的对象obj
	 */
	public static Object invoke(String method_name, Class<?> las) throws Exception {
		Method method = getMethod(las, method_name);
		return method.invoke(las.newInstance(), method.getParameters());
	}
}
